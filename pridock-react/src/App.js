import React, { Component } from 'react'
import {BrowserRouter as Router, Route} from 'react-router-dom'
import {Home} from './views'
import {Navbar, Footer} from './components'

import './App.scss';

class App extends Component {
  render() {
    return (
      <Router>
        <div className="App">
        <Navbar></Navbar>
        <Route  path="/" exact component={Home}/>
        <Footer></Footer>
        </div>
      </Router>
    );
  }
}

export default App;
