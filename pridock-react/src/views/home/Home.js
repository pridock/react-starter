import React from 'react'
import './Home.scss'
import {getAllPosts} from '../../services'

export default class Home extends React.Component{

  constructor(props){
    super(props);
    this.state = {
      posts: []
    }
  }

  componentWillMount() {
    getAllPosts().then(res => {
      console.log(res)
      this.setState({
        posts: res.data
      })
      }).catch(err => {
        console.log(err)
    })
  }


  render() {
    return (
      <div>
        <p>Hello from home!</p>
        <div>{this.state.posts.map((post, index) => <div key={index}>{post.title.rendered}</div> )}</div>
      </div>
    )
  }
}