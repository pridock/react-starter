import axios from 'axios'


//Don't change base url in dev. Make sure to change it in prod!
const http = axios.create({
  baseURL: 'https://cors.io/?https://www.pridock.com/wp-json/wp/v2',
  headers: {
    'Content-Type': 'Application/json'
  }
})

export const getAllPosts = () => {
  return http.get('/posts')
}